﻿README


OPERATIVE SYSTEMS SUPPORTED
        - Windows
        - Linux
        - Mac OS


SOFTWARE REQUIRED
To use this code, the user will need the following software:
        - Python2.7 (We do not garanty it will work in 3.x versions)
                - numpy library for python
                - scipy library for python
                - matplotlib library for python
                - PIL (Python Image Library) library for python


EXECUTABLE SCRIPTS
        - classifier.py
        - stats.py


CONFIGURATION FILES
        - config.py


Has the following parameters:


        # Current users


GROUNDTRUTH = '../test/Apples1/'
CORRESPONDENCE_LIST = 'list.txt’
CLASSIFY_PATH = "../test/Classify/"
OUTFILE = CLASSIFY_PATH + 'classified.txt'


# Advanced users


STEEPEST_REP = 200 # Number of repetitions to avoid local max.
STEEPEST_BEST = 180 # Min heuristic value to validate result.
VERBOSE = False # Shows some information
POINTS = True # If True, draws the points in stats.py


USAGE
- classifier.py


Can be executed directly from a terminal. (for instance python classifier.py in Linux). Reads the configuration file to get a ground truth and a folder of apples to classify and writes to an outfile the correspondence between images and color code.


- stats.py


Can be executed directly from a terminal with one parameter to chose the color space (for instance python classifier.py [option] in Linux).
Option can take the following values:


        1: RGB
        2: HSV
        3: XYZ
        4: CIELAB
        5: RG
        6: CMYK (No plot)
        7: HSL
        8: CMYK without K


If no option is set, a help message will be shown, and  if another option is set, the default execution will be RGB.
Reads the configuration file to get a ground truth, generates a decision function to classify the apples, and generates a report with the apples of the ground truth.