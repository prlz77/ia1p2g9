import os,sys
from copy import deepcopy
from loader import *
from localSearch import *
from config import *
from colorSpaceConverter import *
from classifier import *
import time

import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
import numpy as np

'''
    This function, allows you to draw points in 2D or 3D and a function
    to seaprate the points.
'''

def plotList(ground_truth, function=[[]]):
    
    color=ground_truth[0].coords
    max=list(deepcopy(color))
    min=list(deepcopy(color))
    
    # Creates the figure.
    fig = plt.figure()
    
    # 2D or 3D axis
    if len(color)==2:
        ax = fig.add_subplot(111)
    else:
        ax = p3.Axes3D(fig)

    for item in ground_truth:
        # Inicialize the color of the apple depending on it's code
        if item.color_code==1:
            aux='r'
        elif item.color_code==2:
            aux='g'
        else:
            aux='b'
        if len(item.coords)==2:
            # 2D points
            ax.plot(item.coords[0], item.coords[1], 'o', color=aux)
        else:
            # 3D points
            ax.scatter(item.coords[0],item.coords[1], item.coords[2], color=aux)
            if max[2]<item.coords[2]:
                max[2]=item.coords[2]
            elif min[2]>item.coords[2]:
                min[2]=item.coords[2]
        if max[0]<item.coords[0]:
            max[0]=item.coords[0]
        elif min[0]>item.coords[0]:
                min[0]=item.coords[0]
        if max[1]<item.coords[1]:
            max[1]=item.coords[1]
        elif min[1]>item.coords[1]:
                min[1]=item.coords[1]
                    
    # Draws the equations
    if(function!=[[]]):
        plt.autoscale(enable=False, axis='both', tight=None)
        y=0
        
        z=0
        ax.set_xlim(min[0], max[0])
        ax.set_ylim(min[1], max[1])
        
        for plane in function:
            x = np.linspace(min[0], max[0], 100)
            if(len(color)==3):
                # Planes
                ax.set_zlim(min[2], max[2])
                z=np.linspace(min[2], max[2], 100)
                x, z = np.meshgrid(x, z)
                y=plane[0]*x+plane[1]*z+plane[2]
                ax.plot_surface(x, y, z, color='b', alpha=0.5)
            else:
                j=1
                y=0
                # Lines
                for i in plane:
                    y+=i*(x**(len(plane)-j))
                    j+=1
                if len(color)==2:
                    ax.plot(x, y)
                else:
                    ax.plot(x, y, zs=-1, zdir='z', label='zs=-1, zdir=z')
    # Shows the plot
    plt.show()

'''
    This class, lets you test the different color space, see the points representation (if you want),
    and the point with the two equations. It Will generate a report.
'''

class Stats:
    # Inizialization with the ground truth
    def __init__(self, item_list, min_val, max_val):
        self.apple_list=item_list
        self.min_val = min_val
        self.max_val = max_val
    # Prints a report
    def report(self, information=True):
        count=0
        misses=0
        # Calculates the stadistics.
        for apple in self.apple_list:
            if(apple.color_code==apple.calc_color_code):
                count+=1
            else:
                misses+=1
        success=count*100./len(self.apple_list)
        miss_rate=misses*100./len(self.apple_list)
        # Information of the test, if information variable is set
        if information:
            print "==== REPORT ===="
            print "Total of apples"
            print len(self.apple_list)
            print "Number of successes"
            print count
            print "Number of misses"
            print misses
            print "Success rate"
            print str(success) + "%"
            print "Miss rate"
            print str(miss_rate) + "%"
        
        return (success, miss_rate, count, misses)

    def test(self):
        print "Classification"
        start_time1 = time.time()
        classifier = Classifier(self.apple_list, self.min_val, self.max_val)
        # Calculates the color code, depending on the two equations generated in the Classifier.
        print "Time expended serching the planes: \t"
        print time.time() - start_time1, "seconds"
        start_time2 = time.time()
        for item in self.apple_list:
            item.calc_color_code=classifier.classifiy_apple_RGB(item)
        print "Time expended classify the apples: \t"
        print time.time() - start_time2, "seconds"
        print "Total time expended: \t"
        print time.time() - start_time1, "seconds"
        # If dimension is 2 or 3 represnets the equations.
        if(classifier.dimension==3):
            pla1=[]
            
            pla1.append(-classifier.equation1[0]/classifier.equation1[1])
            pla1.append(-classifier.equation1[2]/classifier.equation1[1])
            pla1.append(-classifier.equation1[3]/classifier.equation1[1])
            pla=[]
            pla.append(pla1)

            pla1=[]
            
            pla1.append(-classifier.equation2[0]/classifier.equation2[1])
            pla1.append(-classifier.equation2[2]/classifier.equation2[1])
            pla1.append(-classifier.equation2[3]/classifier.equation2[1])
            pla.append(pla1)
        elif classifier.dimension==2:
            pla1=[]
                
            pla1.append(-classifier.equation1[0]/classifier.equation1[1])
            pla1.append(-classifier.equation1[2]/classifier.equation1[1])
            pla=[]
            pla.append(pla1)
            
            pla1=[]
            
            pla1.append(-classifier.equation2[0]/classifier.equation2[1])
            pla1.append(-classifier.equation2[2]/classifier.equation2[1])
            pla.append(pla1)
        # Prints a Report
        self.report()
        # Draws.
        if classifier.dimension==2 or classifier.dimension==3:
            plotList(self.apple_list,pla)


'''
    Shows a help message with the use of stats.py.
'''

def show_help():
    print "Usage:"
    print "python stats.py test_number"
    print "Test Number:"
    print "\t1: RGB"
    print "\t2: HSV"
    print "\t3: XYZ"
    print "\t4: CIELAB"
    print "\t5: RG"
    print "\t6: CMYK (No plot)"
    print "\t7: HSL"
    print "\t8: CMYK without K"
    print "It will generate a report"

'''
    Inicialize the ground truth.
'''

def init(csc):
    print "Setting up briefing function..."
    briefing = Briefing(csc)
    briefing.setBriefingFunction(briefing.getMeanColor)
    print "Loading Files..."
    loader = Loader(briefing)
    return loader.getGroundTruth(GROUNDTRUTH)
    
'''
    python stats.py
    Test the space of color that you choose
'''

if __name__== "__main__":
    argv = sys.argv[1:]
    test = None
    # Takes the arguments from the command line
    if len(argv) == 1:
        option = argv[0]
    else:
        show_help()
        exit()
    # Sets the default dimension to 3
    dimension=3
    print "Setting up colorSpaceConverter"
    csc = ColorSpaceConverter()
    # Default option is RGB
    if option == '2':
        csc.setConvertFunction(csc.rgb_to_hsv)
    elif option == '3':
        csc.setConvertFunction(csc.rgb_to_xyz)
    elif option == '4':
        csc.setConvertFunction(csc.rgb_to_cielab)
    elif option == '5':
        csc.setConvertFunction(csc.rgb_to_rg)
        dimension=2
    elif option == '6':
        csc.setConvertFunction(csc.rgb_to_cmyk)
        dimension=4
    elif option == '7':
        csc.setConvertFunction(csc.rgb_to_hsl)
    elif option == '8':
        csc.setConvertFunction(csc.rgb_to_cmy)


    # Sets the ground truth
    gt=init(csc)
    if VERBOSE:
        for item in gt:
            print item.coords
    if POINTS and (dimension==3 or dimension==2):
        # Draws 
        plotList(gt)
    # Generate the stadistics.
    s=Stats(gt, csc.minvalue, csc.maxvalue)
    s.test()


