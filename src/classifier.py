import os,sys
from PIL import Image
from copy import deepcopy
from math import sqrt
from stats import *
from localSearch import *
from config import *
import time

'''
   Evaluates the equation with the coords passed 
'''
def samesign(val1, val2):
    if val1 < 0 and val2 < 0:
        return True
    elif val1 > 0 and val2 > 0:
        return True
    else:
        return False
        
def evaluate(equation, coords):
    Total=0.
    i=0
    for i in range(len(coords)):
        Total+=equation[i]*coords[i]
    Total+=equation[i+1]
    return Total


'''
    Let's you classify the apples choosing the best equation
'''
class Classifier:
    # Inicialization
    def __init__(self, ground_truth, min_val = 0, max_val = 255):
        self.min_val = min_val
        self.max_val = max_val
        self.ground_truth = ground_truth
        self.dimension = len(ground_truth[0].coords)
        self.firstcolor = {"color":0, "signcount":[0,0]}
        self.secondcolor = {"color":0, "signcount":[0,0]}
        self.thirdcolor = {"color":0, "signcount":[0,0]}
        
        
        self.getBestEquation()
    
    def getBestEquation(self):
        # Looks for an equation to separate all the colors
        gt = self.ground_truth
        h = Heuristic(gt, 1)
        ls = LocalSearch(h, self.dimension, self.min_val, self.max_val)
        result1 = ls.steepestAscent(STEEPEST_REP, STEEPEST_BEST)
        h = Heuristic(gt, 2)
        ls = LocalSearch(h, self.dimension, self.min_val, self.max_val)
        result2 = ls.steepestAscent(STEEPEST_REP, STEEPEST_BEST)
        h = Heuristic(gt, 3)
        ls = LocalSearch(h, self.dimension, self.min_val, self.max_val)   
        result3 = ls.steepestAscent(STEEPEST_REP, STEEPEST_BEST)
        
        # Compares the three equations to choose the two
        if max(result1[0], result2[0]) == result1[0]:
            self.equation1 = result1[1]
            minim = result2
            self.firstcolor["color"] = 1
            self.secondcolor["color"] = 2
        else:
            self.equation1 = result2[1]
            minim = result1
            self.firstcolor["color"] = 2
            self.secondcolor["color"] = 1

        if max(minim[0], result3[0]) == minim[0]:
            self.equation2 = minim[1]
            self.thirdcolor["color"] = 3
        else:
            self.equation2 = result3[1]
            self.thirdcolor["color"] = deepcopy(self.secondcolor["color"])
            self.secondcolor["color"] = 3
            
        # If some of the equation can't classify the minimum number of apples.
        if self.equation1 == None or self.equation2 == None:
            print "Some of the equations are None."
            exit()
            
        # Tries apples to know in which side of the equation goes each kind of apples
        for apple in gt:

            if apple.color_code == self.firstcolor["color"]:
                if evaluate(self.equation1, apple.coords)<0:
                    self.firstcolor["signcount"][0] += 1
                else:
                    self.firstcolor["signcount"][1] +=1
            elif apple.color_code == self.secondcolor["color"]:
                if evaluate(self.equation2, apple.coords)<0:
                    self.secondcolor["signcount"][0] += 1
                else:
                    self.secondcolor["signcount"][1] += 1
        
        if self.firstcolor["signcount"][0] > self.firstcolor["signcount"][1]:
            self.firstcolor["sign"] = -1
        else:
            self.firstcolor["sign"] = 1
            
        if self.secondcolor["signcount"][0] > self.secondcolor["signcount"][1]:
            self.secondcolor["sign"] = -1
        else:
            self.secondcolor["sign"] = 1
            
        # Information
        if VERBOSE:
            print "In Classifier.getBestEquation"
            print "Choices are\n\t" + str(result1) + "," + str(result2) + "," + str(result3)
            print "Chosen equations are\n\t" + str(self.equation1) + "," + str(self.equation2)

    
    # Classify an apple depending on the equation
    def classifiy_apple_RGB(self, apple):
        if evaluate(self.equation1, apple.coords) < 0:
            if(self.firstcolor["sign"]==-1):
                return self.firstcolor["color"]
        else:
            if(self.firstcolor["sign"]==1):
                return self.firstcolor["color"]
        if evaluate(self.equation2, apple.coords) < 0:
            if (self.secondcolor["sign"]==-1):
                return self.secondcolor["color"]
            else:
                return self.thirdcolor["color"]
        else:
            if (self.secondcolor["sign"]==1):
                return self.secondcolor["color"]
            else:
                return self.thirdcolor["color"]
            

'''
    python classifier.py
    First calculates the equations using the ground truth, then it classifies a folder with some apples
    in an output file.
'''
if __name__ == '__main__':
    
    print "Setting up colorSpaceConverter"
    csc = ColorSpaceConverter()
    print "Setting up briefing function..."
    briefing = Briefing(csc)
    briefing.setBriefingFunction(briefing.getMeanColor)
    print "Loading Files..."
    loader = Loader(briefing)
    gt = loader.getGroundTruth(GROUNDTRUTH)
    print "Classifying..."
    classifier = Classifier(gt)
    print "Loading apples to classify..."
    to_classify = loader.getFolder(CLASSIFY_PATH)
    
    outfile = open(OUTFILE, 'w')
    for item in to_classify:
        item.color_code=classifier.classifiy_apple_RGB(item)
        outfile.write(str(item) + "\n")
    print "Saving into " + OUTFILE + "..."
    outfile.close()
    







