# Classification
GROUNDTRUTH = '../test/Apples1/'
CORRESPONDENCE_LIST = 'list1.txt'
CLASSIFY_PATH = "../test/Classify/"
OUTFILE = CLASSIFY_PATH + 'classified.txt'

# Debugging

STEEPEST_REP = 200 # Number of repetitions to avoid local max.
STEEPEST_BEST = 180 # Min heuristic value to validate result.
VERBOSE = False # Shows some information
POINTS = True # If True, draws the points in stats.py
