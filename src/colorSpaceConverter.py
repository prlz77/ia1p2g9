import colorsys

''' Converts from RGB to other color spaces '''
class ColorSpaceConverter:
    ''' Does nothing '''
    def rgb_to_rgb(self, r, g, b):
        return (r, g, b)
    
    ''' Constructor. self.convert is the function to change the color space ''' 
    def __init__(self):
        self.convert = self.rgb_to_rgb
        self.minvalue = 0
        self.maxvalue = 255
    
    ''' Passes from 3D RGB space to 2D RG '''
    def rgb_to_rg(self,r,g,b):
        return (r,g)
        
    ''' Sets the function used to convert'''    
    def setConvertFunction(self, function):
        self.convert = function
    
    ''' Converts from RGB to HSV  '''
    def rgb_to_hsv(self, r, g, b):
        '''
        Public: converts RGB values into HSV values. RGB values go from 0 to 255.
        '''
        self.minvalue = 0
        self.maxvalue = 1
        return colorsys.rgb_to_hsv(r/255, g/255, b/255)
    
    ''' Converts from RGB to HSL '''
    def rgb_to_hsl(self, r, g, b):
        r=r/255.
        g=g/255.
        b=b/255.
        var_min=min(r,g,b)
        var_max=max(r,g,b)
        del_max=var_max-var_min
        L=(var_max+var_min)/2
        if del_max==0:
            H=0
            S=0
        else:
            if(L<0.5):
                S=del_max/( var_max + var_min)
            else:
                S = del_max / ( 2 - var_max - var_min )
            del_R = ( ( ( var_max - r ) / 6 ) + ( del_max / 2 ) ) / del_max
            del_G = ( ( ( var_max - g ) / 6 ) + ( del_max / 2 ) ) / del_max
            del_B = ( ( ( var_max - b ) / 6 ) + ( del_max / 2 ) ) / del_max
                
            if  r == var_max:
                H = del_B - del_G
            elif g == var_max:
                H = ( 1 / 3 ) + del_R - del_B
            elif b == var_max:
                H = ( 2 / 3 ) + del_G - del_R
                
            if H < 0:
                H += 1
            if H > 1:
                H -= 1
        self.minvalue = 0
        self.maxvalue = 1
        return H,S,L
    
    ''' Passes from 4D CMYK space to 3D CMY '''
    def rgb_to_cmy(self,r,g,b):
        cyan, magenta, yellow, black = self.rgb_to_cmyk(r, g, b)
        return (cyan, magenta, yellow)
    
    
    def rgb_to_cmyk(self, r, g, b):
        '''
        Public: converts RGB values into CMYK values. RGB values go from 0 to 255.
        '''
        black = min(1-r, 1-g, 1-b)
        if black==1:
            return 0, 0, 0, 1
        cyan = (1-r-black)*1./(1-black)
        magenta = (1-g-black)*1./(1-black)
        yellow = (1-b-black)*1./(1-black)
        self.minvalue = 0
        self.maxvalue = 1
        return (cyan, magenta, yellow, black)

    


    def rgb_to_xyz(self, R, G, B):
        '''
        Convert from RGB to XYZ. RGB values go from 0 to 255.
        '''
        var_R = ( R / 255.)
        var_G = ( G / 255.)
        var_B = ( B / 255.)

        if var_R > 0.04045:
            var_R = ( ( var_R + 0.055 ) / 1.055 ) ** 2.4
        else:
            var_R /= 12.92

        if var_G > 0.04045:
            var_G = ( ( var_G + 0.055 ) / 1.055 ) ** 2.4
        else:
            var_G /= 12.92
        if var_B > 0.04045:
            var_B = ( ( var_B + 0.055 ) / 1.055 ) ** 2.4
        else:
            var_B /= 12.92

        var_R *= 100
        var_G *= 100
        var_B *= 100

        #Observer. = 2 deg, Illuminant = D65
        X = var_R * 0.4124 + var_G * 0.3576 + var_B * 0.1805
        Y = var_R * 0.2126 + var_G * 0.7152 + var_B * 0.0722
        Z = var_R * 0.0193 + var_G * 0.1192 + var_B * 0.9505
        self.minvalue = 0
        self.maxvalue = 180
        return X,Y,Z
    
    

    

    def rgb_to_cielab(self, R, G, B):
        '''
        Convert from RGB to CIE-L*a*b*.
        '''
        X,Y,Z = self.rgb_to_xyz(R,G,B)

        ref_X =  95.047
        ref_Y = 100.000
        ref_Z = 108.883

        var_X = X / ref_X
        var_Y = Y / ref_Y
        var_Z = Z / ref_Z

        if var_X > 0.008856:
            var_X **= ( 1./3. )
        else:
            var_X = ( 7.787 * var_X ) + ( 16. / 116. )
        if var_Y > 0.008856:
            var_Y **= ( 1./3. )
        else:
            var_Y = ( 7.787 * var_Y ) + ( 16. / 116. )
        if var_Z > 0.008856:
            var_Z **= ( 1./3. )
        else:
            var_Z = ( 7.787 * var_Z ) + ( 16. / 116. )

        CIE_L = ( 116 * var_Y ) - 16.
        CIE_a = 500. * ( var_X - var_Y )
        CIE_b = 200. * ( var_Y - var_Z )
        
        self.minvalue = -110
        self.maxvalue = 110
        return CIE_L, CIE_a, CIE_b

    def rgb_to_hsv(self, R, G, B):
        '''
        Convert from RGB to HSV.
        '''
        var_R = ( R / 255.)
        var_G = ( G / 255.)
        var_B = ( B / 255.)

        var_Min = min( var_R, var_G, var_B )    # Min. value of RGB
        var_Max = max( var_R, var_G, var_B )    # Max. value of RGB
        del_Max = var_Max - var_Min             # Delta RGB value

        V = var_Max

        if del_Max == 0:    # This is a gray, no chroma...
            H = 0
            S = 0
        else:               # Chromatic data...
            S = del_Max / var_Max
            
            del_R = ( ( ( var_Max - var_R ) / 6.) + ( del_Max / 2.) ) / del_Max
            del_G = ( ( ( var_Max - var_G ) / 6.) + ( del_Max / 2.) ) / del_Max
            del_B = ( ( ( var_Max - var_B ) / 6.) + ( del_Max / 2.) ) / del_Max
            
            if var_R == var_Max:
                H = del_B - del_G
            elif var_G == var_Max:
                H = ( 1. / 3. ) + del_R - del_B
            elif var_B == var_Max:
                H = ( 2. / 3. ) + del_G - del_R
            
            if H < 0:
                H += 1
            if H > 1:
                H -= 1
        self.minvalue = 0
        self.maxvalue = 1
        return H,S,V
