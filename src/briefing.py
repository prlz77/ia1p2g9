from PIL import Image

class Briefing:
    '''
        Converts all the pixels of an image to one unique
        coord in a color space.
    '''
    def __init__(self, colorSpaceConverter, blackthreshold = 50, whitethreshold=235):
        self.blackthreshold = blackthreshold
        self.whitethreshold = whitethreshold
        self.brief = None
        self.colorSpaceConverter = colorSpaceConverter
    
    '''
        Sets the briefing function to be used when
        self.brief is called
    '''
    def setBriefingFunction(self, func):
        self.brief = func
    

    def getMeanColor(self, pixels):
        '''
        Public: calculates the mean color of the given image, skipping the 
        pixels whose mean value is lower than `blackthreshold` or higher than
        `whitethreshold` (that is, skipping extreme values).
        '''
        
        self.pixels = list(pixels.getdata())
        
        coords = []
        ''' coords will contain the sum of the pixel values
        for each of the n coords of the pixels '''
        for i in range(len(self.colorSpaceConverter.convert(self.pixels[0][0],self.pixels[0][1],self.pixels[0][2]))):
            coords.append(0)
        
        count = 0 # To divide when calculating the average
        for pixel in self.pixels:
            average = (pixel[0] + pixel[1] + pixel[2]) / 3
            if average > self.blackthreshold and average < self.whitethreshold:
                color = self.colorSpaceConverter.convert(pixel[0],pixel[1],pixel[2])
                for index in range(len(coords)):
                    coords[index] += color[index]
                count += 1

        for index in range(len(coords)):
            coords[index] = coords[index] / count
            
        return coords

