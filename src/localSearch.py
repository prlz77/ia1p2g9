from random import randint, random
from copy import deepcopy
from config import *

''' 
    This heuristic divides 1 from n groups using one 
    hiperplane giving higher values as the targeted
    group is in greater part on the opposite side
    of the hiperplane from the other groups.
'''
class Heuristic:
    
    ''' where ground_truth is a list of items. 
    target_color_code is the group to split from the rest'''
    def __init__(self, ground_truth, target_color_code):
        self.ground_truth = ground_truth
        self.t_color_code = target_color_code
    
    def getValue(self, equation):        
        target_group = [0,0] # Number of target_color items at the left and the right of the hiperplane
        rest = [0,0] # Number of the rest of the items at the left and the right of the hiperplane
        
        for item in self.ground_truth:            
            value = 0 # Value will contain the evaluation of the equation with the item coords
            
            # Equation must be in form: Ax +By + Cz + ... + N = 0
            for i in range(len(item.coords)):
                value += item.coords[i] * equation[i]
                
            value += equation[-1] # Last equation term
            
            # Classify            
            if value < 0:
                if int(item.color_code) == int(self.t_color_code):
                    target_group[0] += 1
                else:
                    rest[0] += 1
            elif value > 0:
                if int(item.color_code) == int(self.t_color_code):
                    target_group[1] += 1
                else:
                    rest[1] += 1
        if VERBOSE:
            print "La heuristica classifica " + str(target_group) + "del color dessitjat"
            print "La heuristica classifica " + str(rest) + "de la resta"
        if max(target_group[0], target_group[1]) == target_group[0]:
            return target_group[0] + rest[1]
        else:
            return target_group[1] + rest[0]

         
      
class LocalSearch:
    ''' 
        Searches a max. of the heuristic calculating all the possible planes
        in the space.
        Constructor receives: 
            heuristic : the heuristic to use
            dimension : dimension of the color space used.
            min_value: min value in the space (which is 0 for RGB)
            max_value (which is 255 for RGB). If different max_values use 
            the greatest one. the same for min_value 
    '''
    def __init__(self, heuristic, dimension, min_value, max_value):
        self.heuristic = heuristic
        self.dimension = dimension
        self.max_value = max_value
        self.min_value = min_value

    ''' Gets a random hiperplane '''
    def getRandomState(self):
        randomstate = []
        for i in range(self.dimension):
            randnum = random()
            if randint(0,1) == 0:
                randnum = -randnum
            randomstate.append(randnum)

        randomstate.append(randint(self.min_value,self.max_value - 1) + random())

        return randomstate

    ''' Receives current hiperplane and returns all possible unitary variations '''
    def expand(self,state):
        expanded = []
        for i in range(len(state)):
            newstate1 = deepcopy(state)
            newstate2 = deepcopy(state)
            if abs(self.max_value) > 10:
                newstate1[i] += 1
                newstate2[i] -= 1
            else:
                newstate1[i] += 0.1
                newstate2[i] -= 0.1        
            expanded.append(newstate1)
            expanded.append(newstate2)
        return expanded

    ''' Uses the heuristic to select the best hipeplane in a list '''
    def best(self,state_list):
        max_value = 0
        max_index =  0
        for i in range(len(state_list)):
            h = self.heuristic.getValue(state_list[i])
            if h > max_value:
                max_value = h
                max_index = i
                
        return state_list[max_index]
    
    
    '''
    Steepest Ascent Algorithm
        max_iter is the max number of trials to find the absolute max.
        best_case is the lowest result we want to obtain with the heuristic
    '''
    def steepestAscent(self, max_iter, best_case):
        numiter = 0
        repeat = True
        final_state = None

        while repeat:
            local_max = False
            current_state = self.getRandomState()
            if VERBOSE:
                print "Current state: " + str(current_state)
            while not local_max:
                e = self.expand(current_state)
                if VERBOSE:
                    print "Current state expansion: " + str(e)
                successor = self.best(e)
                
                if  self.heuristic.getValue(successor) > self.heuristic.getValue(current_state):
                    current_state = successor
                else:
                    local_max = True
                    
            numiter += 1
            
            h = self.heuristic.getValue(current_state)
            
            if h > best_case:
                best_case = h
                final_state = current_state
            
            if numiter == max_iter:
                repeat = False
            
        return best_case, final_state

