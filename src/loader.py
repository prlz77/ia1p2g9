import os,sys
from PIL import Image
from briefing import Briefing
from config import *

class Item:
    '''
        Basic item class to classify.
        Constructor receive:
            name : name of the item
            coords: coords in a color space
            color_code: the color of the item (None by default)
    '''
    def __init__(self, name, coords, color_code=None):
        self.name = name
        self.color_code = color_code
        self.coords = coords
        self.calc_color_code = None
        
    ''' Overload of str function '''
    def __str__(self):
        return self.name + " " + str(self.color_code)

'''
   Class Loader. Loads lists of images and returns lists
   of items in an appropiate colorspace.
   Constructor recieves:
        briefer: Briefing instance
   
'''
class Loader:
    def __init__(self, briefer):
        self.briefer = briefer
        
    ''' Reads a list of items and loads them with the known color code '''
    def getGroundTruth(self,listpath):
        item_list = []
        ground_truth = open(listpath + CORRESPONDENCE_LIST, 'r')
        
        for item in ground_truth:
            split_item = item.split(" ")
            image = Image.open(listpath + split_item[0]+'.png')
  
            color_code = int(split_item[-1].split('=')[1][0])
            coords = self.briefer.brief(image) # function that receives an image and returns a coord list.
            it = Item(split_item[0], coords, color_code)
            item_list.append(it)
        
        ground_truth.close()
        
        return item_list
    
    ''' Loads all images in a folder and returns an item list '''
    def getFolder(self, path):
        item_list = []
        for filename in os.listdir(path):
            if filename.split(".")[1] in ["png", "jpg", "bmp", "tiff", "jpeg", "gif"]:
                image = Image.open(path + filename)
                coords = self.briefer.brief(image) # function that receives an image and returns a coord list.
                it = Item(filename.split('.')[0], coords)
                item_list.append(it)
        return item_list

